from pumperbot import scraper
from notifiers import messenger
from model import TopPicksData
from model import CoinCapData
from time import sleep
import schedule
import pandas as pd

message = ''


def get_pump_data():
    cid_symobls = CoinCapData.get_all_cids_and_symbols()

    lastest_toppicksdate = TopPicksData.getAllDates()
    if lastest_toppicksdate:
        toppicks_from_db = TopPicksData.getTopPicksByDate(
            lastest_toppicksdate[-1])

        toppicks = ['ZEC']  # add pumping coin here for testing
        for pick in toppicks_from_db:
            toppicks.append(cid_symobls[pick])

        data = scraper.getPumpData(search=toppicks)
        data['dataframe'].to_pickle('PUMPER_DATA.pickle')

        global message
        message = scraper.createNewWatchlist(data)
        print('LATEST TOP PICKS DATE: {}'.format(
            lastest_toppicksdate[0].time()))
        print('TOP PICKS: {}'.format(toppicks))

    else:
        print('no toppicksdata')


def send_pump_message():
    messenger.sendMessage(message)


def get_pumper_data_pickle():
    return pd.read_pickle('PUMPER_DATA.pickle')


if __name__ == '__main__':
    schedule.every(.5).minutes.do(get_pump_data)
    schedule.every(1).minutes.do(send_pump_message)

    while True:
        schedule.run_pending()
        sleep(1)
