from datetime import date, datetime
from deco import concurrent, synchronized

from logger import GetLogger

from model import CoinCapData, CoinCapTwitterData
from model import ExchangeData, ExchangeTwitterData

from time import time as tm
from top_picks import add_top_coins_to_database
from parsers.twitter import getUserStats

_logger = GetLogger()


def getCurrentDateTime():
    dt = datetime.utcnow()
    return datetime(dt.year, dt.month, dt.day, dt.hour, 30 * (dt.minute // 30))


@concurrent
def getCoinCapTwitterData(time):
    for cid, twitter_handle in CoinCapData.get_all_twitter_names().items():
        data = {**{'cid': cid}, **getUserStats(twitter_handle), **time}
        CoinCapTwitterData.addData(**data)
        _logger.info('{} twitter data recorded...'.format(data['cid']))


@concurrent
def getExchangeTwitterData(time):
    for name, twitter_handle in ExchangeData.get_all_twitter_names().items():
        data = {**{'name': name}, **getUserStats(twitter_handle), **time}
        ExchangeTwitterData.addData(**data)
        _logger.info('{} twitter data recorded...'.format(data['name']))


@synchronized
def parseAllTwitterData(_time):
    time = {'created_date': _time}
    getCoinCapTwitterData(time)
    getExchangeTwitterData(time)


if __name__ == '__main__':
    start_time = tm()
    start_datetime = getCurrentDateTime()
    parseAllTwitterData(start_datetime)
    _logger.info(
        'twitter-data-parser took {} minutes.'.format((tm() - start_time) / 60))

    if CoinCapTwitterData.getTableSize() > 168:  # 7 days of 60 minutes samples = 168
        _logger.info('Twitter data > 7 Days. Running Top_Coins.')
        add_top_coins_to_database(start_datetime, new_data=True)
    else:
        _logger.info(
            'Twitter data < 7 Days. Unable to run Top_Coins.')
