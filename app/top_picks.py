from datetime import datetime
import pandas as pd
from collections import Counter

from filters import getSogFilterCoins, getSogFilterPlusCoins, getBillionFilterCoins, get500MillionFilterCoins, getLowVolFilterShitCoins
from filters import getLessThan100MillionFilterCoins, getLessThan10MillionFilterCoins
from filters import getBillionFilterCoinsNoBinace, get500MillionFilterCoinsNoBinace
from logger import GetLogger
from model import CoinCapTwitterData, TopPicksData
from data_controller import create_pickle

from os import path, makedirs

_logger = GetLogger()

BASE_PATH = path.dirname(path.abspath(__file__))
PICKLE_PATH = path.join(BASE_PATH, 'TOP_PICKS.pickle')


def getCurrentDateTime():
    return datetime.utcnow()


def get_pickle():
    return pd.read_pickle(PICKLE_PATH)


def print_list(dframe, **kwargs):
    min_tweet_size = kwargs['min_tweet_size']
    resample_size = kwargs['resample_size']
    return_size = kwargs['return_size']

    latest = dframe[-1:]
    bools = (latest > min_tweet_size).any(axis=0)
    keep = list(latest.loc[:, bools].columns)
    pct = dframe[keep]
    pct = pct.resample(resample_size).mean().pct_change()

    row = pct[-1:].reset_index().drop('index', axis=1)
    row = row.melt(var_name='coin')
    row = row.sort_values('value').rename(columns={'value': 'percent_change'})
    row['percent_change'] = row['percent_change'] * 100
    return list(row['coin'].tail(return_size))


def create_pickle():
    # [:100]# used for testing.
    coins = [x.cid_id for x in CoinCapTwitterData.getDistictCoinsCids()]
    df = pd.DataFrame()
    for count, x in enumerate(coins):
        followers = []
        index = []
        for y in CoinCapTwitterData.getFollowersByCid(x):
            followers.append(y.followers)
            index.append(y.created_date)
        if count == 0:
            df = pd.DataFrame(index=index, data=followers, columns=[x])
        else:
            df = df.merge(pd.DataFrame(
                index=index, data=followers, columns=[x]), right_index=True, left_index=True, how='outer')

        _logger.info('pickling {} out of {} coins processed.'.format(
            count + 1, len(coins)))

    df.fillna(method='ffill', inplace=True)
    df.fillna(method='bfill', inplace=True)
    df.to_pickle(PICKLE_PATH)
    _logger.info('Pickle complete')


def get_number_one(filter):
    df = get_pickle()[filter()]
    hitterlist = []
    for x in ['6H', '12H', '1D', '2D', '3D', '4D', '5D', '6D', '7D']:
        hitterlist.append(print_list(df, min_tweet_size=100,
                                     resample_size=x, return_size=1)[0])
    print(filter.__name__, set(hitterlist))
    return set(hitterlist)


def add_top_coins_to_database(date, new_data=False):
    if new_data:
        create_pickle()
    filters = [getSogFilterCoins, getSogFilterPlusCoins, getBillionFilterCoins, get500MillionFilterCoins, getLowVolFilterShitCoins,
               getLessThan100MillionFilterCoins, getBillionFilterCoinsNoBinace, get500MillionFilterCoinsNoBinace]

    created = date

    top_coins = []
    for f in filters:
        top_coins += get_number_one(f)
    data = list(set(top_coins))

    for d in data:
        TopPicksData.addData(**{'cid': d, 'created_date': created})

if __name__ == '__main__':
    # create_pickle()
    add_top_coins_to_database(getCurrentDateTime())
