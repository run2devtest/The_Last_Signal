import ccxt
from time import sleep


from ccxt.base.errors import AuthenticationError, ExchangeError, RequestTimeout, ExchangeNotAvailable, DDoSProtection
# from config import getApiKeys, getSleepTime, getSendAlertSetting
from scanner.model import Exchanges, Symbols
# from bot import sendNewCoinMessage, sendHaltMessage, sendStartMessage
from requests.exceptions import HTTPError
# SEND_ALERT = getSendAlertSetting()
import atexit


def add_db_exchange(exchange):
    data = {'xid': exchange.id, 'name': exchange.name}
    Exchanges.addExchange(**data)


def add_db_symbols(exchange):
    symbols = list(exchange.symbols)
    for symbl in symbols:
        data = {'xid': exchange.id, 'symbol': symbl}
        if Symbols.addData(**data):
            print(exchange.id, symbl, 'new')
        #     if SEND_ALERT:
        #         sendNewCoinMessage(exchange.id, symbl)
        #         sleep(6)

        # else:
        #     if SEND_ALERT:
        #         #print(exchange.id, symbl, 'old')
        #         pass


# @atexit.register
# def goodbye():
#     sendHaltMessage()


def main():
    print('Running...')
    exchanges = {}

    for id in ccxt.exchanges:
        exchange = getattr(ccxt, id)
        exchanges[id] = exchange({'enableRateLimit': True})

    tuples = list(ccxt.Exchange.keysort(exchanges).items())

    authentication_errors = []
    exchange_errors = []
    request_timeouts = []
    exchange_unavailable = []
    exchange_http_error = []

    for (id, params) in tuples:
        print('Processing {}'.format(id))
        exchange = exchanges[id]
        try:
            print('Loading {}'.format(id))
            exchange.load_markets()
            symbols = list(exchange.symbols)
            if symbols:
                add_db_exchange(exchange)
                add_db_symbols(exchange)
        except AuthenticationError:
            authentication_errors.append(id)
            # raise
        except ExchangeError:
            exchange_errors.append(id)

        except RequestTimeout:
            request_timeouts.append(id)

        except ExchangeNotAvailable:
            exchange_unavailable.append(id)
        except TypeError:
            exchange_http_error.append(id)
        except HTTPError:
            print('HTTP ERROR on ()'.format(id))
        except DDoSProtection:
            exchange_unavailable.append(id)

    print('AuthenticationErrors: {}'.format(authentication_errors))
    print('ExchangeErrors: {}'.format(exchange_errors))
    print('Request Timeouts: {}'.format(request_timeouts))
    print('Exchange Unavailable: {}'.format(exchange_unavailable))
    print('Exchange HTTPError: {}'.format(exchange_http_error))

if __name__ == '__main__':
    # sendStartMessage()
    while True:
        main()
        sleep(int(getSleepTime()) * 60)
