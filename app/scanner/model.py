from datetime import datetime
from peewee import *

db = PostgresqlDatabase('last_signal_scanner')


class BaseModel(Model):

    class Meta:
        database = db


class Exchanges(BaseModel):
    xid = CharField(unique=True)

    name = CharField(unique=True)

    created_date = DateTimeField()
    last_updated = DateTimeField(null=True)

    def addExchange(**kwargs):
        try:
            time = {'created_date': getCurrentDateTime(
            ), 'last_updated': getCurrentDateTime()}
            updatedkwargs = {**kwargs, **time}

            Exchanges.create(**updatedkwargs)
            # _logger.info(
            #     '{} added to exchange-data-table...'.format(kwargs['name']))

        except IntegrityError:
            db.rollback()
            Exchanges.updateExchange(**kwargs)
            # _logger.info(
            #     '{} already exists in exchange-data-table...'.format(kwargs['name']))

    def updateExchange(**kwargs):
        try:
            name_query = kwargs.pop('name')
            time = {'last_updated': getCurrentDateTime()}
            updatedkwargs = {**kwargs, **time}
            query = Exchanges.update(
                **updatedkwargs).where(Exchanges.name == name_query)
            query.execute()
            # _logger.info(
            #     '{} updated in exchange-data-table...'.format(name_query))

        except Exchanges.DoesNotExist:
            # _logger.info(
            #     '{} doesnot exist in exchange-data-table...'.format(kwargs['name']))
            Exchanges.addExchange(**kwargs)


class Symbols(BaseModel):
    xid = ForeignKeyField(Exchanges, to_field='xid')
    symbol = CharField()

    created_date = DateTimeField()

    def addData(**kwargs):
        time = {'created_date': getCurrentDateTime()}
        updatedkwargs = {**kwargs, **time}
        try:
            Symbols.get(Symbols.xid_id == kwargs[
                        'xid'], Symbols.symbol == kwargs['symbol'])
            return False

        except Symbols.DoesNotExist:
            Symbols.create(**updatedkwargs)
            return True


tables = [Exchanges, Symbols]


def getCurrentDateTime():
    return datetime.utcnow()


def createTables():
    db.create_tables(tables, True)


def dropTables():
    db.drop_tables(tables, True)


def main():
    dropTables()
    createTables()
    print('Tables Cleaned...')


if __name__ == '__main__':
    main()
