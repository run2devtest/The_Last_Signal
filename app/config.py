import configparser

from os import path

BASE_PATH = path.dirname(path.abspath(__file__))
CONFIG_FILE = path.join(BASE_PATH, 'settings.ini')


def createConfig():
    config = configparser.ConfigParser()
    config['SETTINGS'] = {'PLAYSOUND': False,
                          'CYCLETIME': 60, 'CYCLE_SLEEPTIME': 60, 'FIRST_RUN': True}

    with open(CONFIG_FILE, 'w') as configfile:
        config.write(configfile)


def readConfig():
    config = configparser.ConfigParser()
    config.read(CONFIG_FILE)
    return config


def getSoundSetting():
    config = readConfig()
    return config['SETTINGS'].getboolean('PLAYSOUND')


def getCycleTime():
    config = readConfig()
    return int(config['SETTINGS']['CYCLETIME'])


def getCycleSleepTime():
    config = readConfig()
    return int(config['SETTINGS']['CYCLE_SLEEPTIME'])


def getFirstRun():
    config = readConfig()
    return config['SETTINGS'].getboolean('FIRST_RUN')


if __name__ == '__main__':
    createConfig()
    print('Settings Set to Default.')
