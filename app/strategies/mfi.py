import pandas as pd


class MFI():

    def __init__(self, df):
        self.data = df
        self.periods = 30
        self.calculate_money_flow()
        self.create_signal_data()

    def get_money_flow_data(self):
        return self.data

    def calculate_money_flow(self):
        df = self.data

        df.reset_index(inplace=True)
        TP = (df['high'] + df['low'] + df['close']) / 3  # Typical Price
        VOL = df['volume']  # Raw Volume
        PosMF = []  # Positive Money Flow
        NegMF = []  # Negative Money Flow

        for idx, p in enumerate(TP.pct_change()):
            if p > 0:
                PosMF.append(TP[idx] * VOL[idx])
                NegMF.append(0)
            else:
                NegMF.append(TP[idx] * VOL[idx])
                PosMF.append(0)

        PosMF = pd.Series(PosMF).rolling(window=self.periods).sum()
        NegMF = pd.Series(NegMF).rolling(window=self.periods).sum()

        MFR = pd.Series(PosMF / NegMF)  # Money Flow Ratio
        MFI = pd.Series(100 - 100 / (1 + MFR), name='MFI')

        df = df.join(MFI)
        df.set_index('date', inplace=True)
        self.data = df

    def create_signal_data(self):
        if 'MFI' in self.data.columns:

            self.data['MFI_OverBought'] = 80
            self.data['MFI_Center'] = 50
            self.data['MFI_OverSold'] = 20
            self.data['MFI_Over_Signal'] = 0

            self.data.loc[self.data['MFI'] > self.data[
                'MFI_OverBought'], 'MFI_Over_Signal'] = 1
            self.data.loc[self.data['MFI'] < self.data[
                'MFI_OverSold'], 'MFI_Over_Signal'] = -1
