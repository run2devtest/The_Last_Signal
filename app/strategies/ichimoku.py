class Ichimoku():

    def __init__(self, df):
        self.data = df

        self.conversion_periods = 10
        self.base_periods = 30
        self.leading_span_periods = 60
        self.displacement = 30

        self.calculate_tenkans()
        self.calculate_kinjuns()
        self.calculate_leading_spanA()
        self.calculate_leading_spanB()

        self.create_last_close_kijun_tenken()

        self.calculate_candle_colors()

    def get_ichimoku_data(self):
        return self.data

    def calculate_donchain(self, periods):
        return (self.data['low'].rolling(periods).min() + self.data['high'].rolling(periods).max()) / 2

    # Tenkan Calculations

    def calculate_tenkans(self):
        self.data['Tenken'] = self.calculate_donchain(
            self.conversion_periods)
        self.data['Tenken_DBL'] = self.calculate_donchain(
            self.conversion_periods * 2)
        self.data['Tenken_HALF'] = self.calculate_donchain(
            self.conversion_periods // 2)

    # Kinjun Calculationis

    def calculate_kinjuns(self):
        self.data['Kinjun'] = self.calculate_donchain(self.base_periods)
        self.data['Kinjun_DBL'] = self.calculate_donchain(
            self.base_periods * 2)
        self.data['Kinjun_HALF'] = self.calculate_donchain(
            self.base_periods // 2)

    def calculate_leading_spanA(self):
        self.data['Leading_Span_A'] = self.data[
            ['Tenken', 'Kinjun']].mean(axis=1).shift(self.displacement)
        self.data['Leading_Span_A_DBL'] = self.data[
            ['Tenken_DBL', 'Kinjun_DBL']].mean(axis=1).shift(self.displacement)
        self.data['Leading_Span_A_HALF'] = self.data[
            ['Tenken_HALF', 'Kinjun_HALF']].mean(axis=1).shift(self.displacement)

    def calculate_leading_spanB(self):
        self.data['Leading_Span_B'] = self.calculate_donchain(
            self.leading_span_periods).shift(self.displacement)
        self.data['Leading_Span_B_DBL'] = self.calculate_donchain(
            self.leading_span_periods * 2).shift(self.displacement)
        self.data['Leading_Span_B_HALF'] = self.calculate_donchain(
            self.leading_span_periods // 2).shift(self.displacement)

    def create_last_close_kijun_tenken(self):
        self.data['Last_Kinjun_DBL'] = self.data['Kinjun_DBL'].shift(1)
        self.data['Last_Tenken_DBL'] = self.data['Tenken_DBL'].shift(1)
        self.data['Last_Close'] = self.data['close'].shift(1)

    def create_candles(row):
        close = row['close']
        last_close = row['Last_Close']

        leadingSpanA = row['Leading_Span_A']
        doubleSpanA = row['Leading_Span_A_DBL']
        halfSpanA = row['Leading_Span_A_HALF']

        leadingSpanB = row['Leading_Span_B']
        doubleSpanB = row['Leading_Span_B_DBL']
        halfSpanB = row['Leading_Span_B_HALF']

        signal1 = True if (last_close < leadingSpanA) and (
            last_close < doubleSpanA) and (last_close < halfSpanA) else None
        signal2 = True if (last_close < leadingSpanA) and (
            last_close < halfSpanA) else None
        signal3 = True if (last_close < halfSpanA) else None

        signal4 = True if (last_close < leadingSpanB) and (
            last_close < doubleSpanB) and (last_close < halfSpanB) else None
        signal5 = True if (last_close < leadingSpanB) and (
            last_close < halfSpanB) else None
        signal6 = True if (last_close < halfSpanB) else None

        signalA1 = signal1 and signal4 or signal1 and signal5 or signal1 and signal6
        signalA2 = signal2 and signal4 or signal2 and signal5 or signal2 and signal6
        signalA3 = signal3 and signal4 or signal3 and signal5 or signal3 and signal6

        signalA = signalA1 or signalA2 or signalA3

        signal7 = True if (close > leadingSpanA) and (
            close > doubleSpanA) and (close > halfSpanA) else None
        signal8 = True if (close > leadingSpanA) and (
            close > halfSpanA) else None
        signal9 = True if (close > halfSpanA) else None

        signal10 = True if (close > leadingSpanB) and (
            close > doubleSpanB) and (close > halfSpanB) else None
        signal11 = True if (close > leadingSpanB) and (
            close > halfSpanB) else None
        signal12 = True if (close > halfSpanB) else None

        signalB1 = signal7 and signal10 or signal7 and signal11 or signal7 and signal12
        signalB2 = signal8 and signal10 or signal8 and signal11 or signal8 and signal12
        signalB3 = signal9 and signal10 or signal9 and signal11 or signal9 and signal12
        signalB = signalB1 or signalB2 or signalB3

        signal = signalA and signalB

        bigkijun_last = row['Last_Kinjun_DBL']
        bigtenken_last = row['Last_Tenken_DBL']

        bigkijun = row['Kinjun_DBL']
        bigtenken = row['Tenken_DBL']

        bull_cross = True if ((bigtenken >= bigkijun) & (
            bigtenken_last < bigkijun_last)) else False

        bear_cross = True if ((bigtenken <= bigkijun) & (
            bigtenken_last > bigkijun_last)) else False

        if (signal or bull_cross):
            return 'yellow'
        if bear_cross:
            return 'white'
        elif signalB:
            return 'green'
        else:
            return 'red'

    def calculate_candle_colors(self):
        self.data['candle_color'] = self.data.apply(
            Ichimoku.create_candles, axis=1)

    def get_price_cloud_relation(self):
        row = self.data.tail(1)
        close = float(row['close'])

        lspanA = float(row['Leading_Span_A'])
        lspanA_DBL = float(row['Leading_Span_A_DBL'])
        lspanA_HALF = float(row['Leading_Span_A_HALF'])

        lspanB = float(row['Leading_Span_B'])
        lspanB_DBL = float(row['Leading_Span_B_DBL'])
        lspanB_HALF = float(row['Leading_Span_B_HALF'])

        cloud_bottom = min([lspanA, lspanA_DBL, lspanA_HALF,
                            lspanB, lspanB_DBL, lspanB_HALF])
        cloud_top = max([lspanA, lspanA_DBL, lspanA_HALF,
                         lspanB, lspanB_DBL, lspanB_HALF])

        price_below_cloud = close < cloud_top and close < cloud_bottom
        price_above_cloud = close > cloud_top and close > cloud_bottom
        price_inside_cloud = close <= cloud_top and close >= cloud_bottom

        if price_below_cloud:
            return 'Below'
        elif price_above_cloud:
            return 'Above'
        elif price_inside_cloud:
            return 'Inside'
        else:
            return 'Unknown'
