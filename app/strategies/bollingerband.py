import pandas as pd


class BB():

    def __init__(self, df):
        self.data = df
        self.periods = 30
        self.length = 30
        self.mult = 2.0
        self.calculate_bollinger_width()

    def calculate_bollinger_width(self):
        df = self.data

        MA = pd.Series(df['close'].rolling(window=self.periods).mean())
        MSD = pd.Series(df['close'].rolling(
            window=self.periods).std() * self.mult)

        BB_UPPER = pd.Series(MA + MSD)
        BB_LOWER = pd.Series(MA - MSD)

        BB_WIDTH = pd.Series(((BB_UPPER - BB_LOWER) / MA)
                             * 100, name='BB_WIDTH')

        df = df.join(BB_WIDTH)
        self.data = df

    def get_bollinger_width(self):
        return self.data
