import requests
from bs4 import BeautifulSoup
from decimal import Decimal
import pandas as pd
import time
from pumperbot.twitter_mentions import get_top_mentions


def convertDollars(x):
    try:
        x = int(x.replace('$', '').replace(',', '').replace('.', ''))
        return x
    except ValueError:
        return 0


def convertPercent(x):
    try:
        x = float(x.replace('%', ''))
        return x
    except ValueError:
        return 0


def convert_change_btc(cell):
    try:
        return float(cell['data-btc'])
    except KeyError:
        return 0.0
    except ValueError:
        return 0.0


def convert_change_usd(cell):
    try:
        return float(cell['data-usd'])
    except KeyError:
        return 0.0
    except ValueError:
        return 0.0


def convert_supply(cell):
    return cell.text.replace(' ', '').replace('\n', '')


def convert_btc(cell):
    return Decimal(cell['data-btc'])


def convert_vol_btc(cell):
    try:
        return int(float(cell['data-btc']))
    except ValueError:
        return 0


def convert_usd(cell):
    try:
        return int(float(cell['data-usd']))
    except ValueError:
        return 0


def get_row_info(row):
    data = dict()
    # data['rank'] = int(row[0].text.replace(' ', '').replace('\n', ''))

    data['symbol'] = row[1].span.a.text

    # data['marketcap_btc'] = convert_btc(row[3])
    # data['marketcap_usd'] = row[3]['data-usd']

    # data['price_btc'] = convert_btc(row[4].a)
    # data['price_usd'] = row[4].a['data-usd']

    # data['supply'] = convert_supply(row[5])

    data['vol_btc'] = convert_vol_btc(row[6].a)
    data['vol_usd'] = convert_usd(row[6].a)

    data['1h_btc%'] = convert_change_btc(row[7])
    data['1h_usd%'] = convert_change_usd(row[7])

    data['24h_btc%'] = convert_change_btc(row[8])
    data['24h_usd%'] = convert_change_usd(row[8])

    data['7d_btc%'] = convert_change_btc(row[9])
    data['7d_usd%'] = convert_change_usd(row[9])
    return data


def filter_twitter(x, twitter_list):
    twitter_coins = twitter_list
    if x in twitter_coins:
        return True
    return False


def get_row_data():
    url = 'https://coinmarketcap.com/all/views/all/'
    resp = requests.get(url)
    soup = BeautifulSoup(resp.content, 'lxml')
    table = soup.find('table')
    rows = table.find_all('tr')

    rows.pop(0)
    return rows


def getPumpData(search=None):
    listofcoins = []

    for x in get_row_data():
        listofcoins.append(get_row_info(x.find_all('td')))

    df = pd.DataFrame(listofcoins)

    df.sort_values(['1h_btc%'], ascending=True, inplace=True)
    df = df[df['vol_btc'] > 1]

    mean_volume = df['vol_btc'].mean()
    threshold_volume = round(mean_volume / 32)

    mean_1hpercent = df['1h_btc%'].abs().mean() / 2
    mean_24hpercent = df['24h_btc%'].abs().mean() / 2
    mean_7dpercent = df['7d_btc%'].abs().mean()
    # print(mean_1hpercent)
    # print(mean_24hpercent)
    # print(mean_7dpercent)

    df = df[(df['vol_btc'] > (threshold_volume))]
    df = df[df['1h_btc%'] > mean_1hpercent]
    df = df[df['24h_btc%'] > mean_24hpercent]
    df = df[df['7d_btc%'] < 1000]

    twitter_list = get_top_mentions()

    df['twitr'] = df['symbol'].apply(filter_twitter, args=(twitter_list,))

    columns = ['symbol', 'vol_btc', '1h_btc%',
               '24h_btc%', '7d_btc%', 'twitr']
    df = df[columns]

    if search:
        df = df[df['symbol'].isin(search)]
        return {'dataframe': df, 'mean_vol': mean_volume, 'threshold_vol': threshold_volume, 'threshold_1H': mean_1hpercent, 'threshold_24H': mean_24hpercent, 'threshold_7D': mean_7dpercent}
    return {'dataframe': df, 'mean_vol': mean_volume, 'threshold_vol': threshold_volume, 'threshold_1H': mean_1hpercent, 'threshold_24H': mean_24hpercent, 'threshold_7D': mean_7dpercent}


def createNewWatchlist(data_dict):
    df = data_dict['dataframe']
    mean_volume = data_dict['mean_vol']
    threshold_volume = data_dict['threshold_vol']

    raw_string = 'Crypto Market BTC Mean Volume: {} \n'.format(
        int(mean_volume))
    raw_string += 'Volume Threshold: {} \n'.format(int(threshold_volume))
    raw_string += '1H Threshold: {:.2f}% \n'.format(data_dict['threshold_1H'])
    raw_string += '24H Threshold: {:.2f}% \n'.format(
        data_dict['threshold_24H'])
    # raw_string += '7D (Absolute Value) Threshold: {}% \n'.format(
    # int(data['threshold_7D']))
    raw_string += '*' * \
        len('symbol  vol_btc  1h_btc%  24h_btc%  7d_btc% twitr') + '\n'

    raw_string += df.to_string(index=False)
    return raw_string

if __name__ == '__main__':
    social_pumper = {'vechain': 'VEN', 'yee': 'YEE', 'iostoken': 'IOST', 'buzzcoin': 'BUZZ',
                     'rimbit': 'RBT', 'reftoken': 'REF', 'syndicate': 'SYNX', 'bancor': 'BNT', 'data': 'DTA', 'monacoin': 'MONA'}

    social_pumper = list(social_pumper.values())

    data = getPumpData(search=social_pumper)
    print(createNewWatchlist(data))
    print('done')
