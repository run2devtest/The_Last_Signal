from crontab import CronTab
from os import path
import getpass

from coinmarket_scraper import writeDataToCoinCapdatabase
from exchange_scraper import getAllExchangeInfo
from model import cleanTables
from logger import GetLogger
from config import getFirstRun


_logger = GetLogger()

BASE_PATH = path.dirname(path.abspath(__file__))
COINMARKET_SCARPER = path.join(BASE_PATH, 'coinmarket_scraper.py')
EXCHANGE_SCAPER = path.join(BASE_PATH, 'exchange_scraper.py')
TWITTERDATA_PARSER = path.join(BASE_PATH, 'twitter_scraper.py')
CRONLOG = path.join(BASE_PATH, 'cron.log')

FILES = [COINMARKET_SCARPER, EXCHANGE_SCAPER, TWITTERDATA_PARSER]


def check_files():
    for file in FILES:
        if not path.isfile(file):
            return False
    return True

def run():
    cron_cmd = 'cd {} && /usr/bin/python3 {} >{}1 2>&1'

    username = getpass.getuser()
    _logger.info('Username: {}'.format(username))
    if check_files():
        taskr = CronTab(user=username)

        job1 = taskr.new(command=cron_cmd.format(BASE_PATH, COINMARKET_SCARPER, CRONLOG))
        job2 = taskr.new(command=cron_cmd.format(BASE_PATH, EXCHANGE_SCAPER, CRONLOG))
        job3 = taskr.new(command=cron_cmd.format(BASE_PATH, TWITTERDATA_PARSER, CRONLOG))
        if getFirstRun():
            _logger.info('running the_sauce script for the first time...')
            cleanTables()
            writeDataToCoinCapdatabase()
            getAllExchangeInfo()

        job1.day.every(1)
        job1.hour.on(0)
        job1.minute.on(0)

        job2.day.every(1)
        job2.hour.on(0)
        job2.minute.on(0)

        job3.hour.every(1)
        job3.minute.on(0)

        taskr.write()
        _logger.info('{}written to crontab.'.format(taskr.render()))


if __name__ == '__main__':
    run()
