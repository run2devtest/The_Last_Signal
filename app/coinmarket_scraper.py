from bs4 import BeautifulSoup
from coinmarketcap import Market
from datetime import datetime
from model import CoinCapData
from decimal import Decimal
from time import time, sleep
from logger import GetLogger
from parsers.statistics import getStatistics

import requests

_logger = GetLogger()


def convertStr_Decimal(str_arg):
    """Converts String to Decimal Obj."""
    if str_arg:
        return Decimal(str_arg)
    return None


def convertStr_Decimal_Int(str_arg):
    """Converts Str to Decimal Obj to Int."""
    if str_arg:
        return int(convertStr_Decimal(str_arg))
    return None


def convertSatoshi(sat_float):
    """Converts float to Satoshis."""
    if sat_float:
        return int(convertStr_Decimal(sat_float) * 100000000)
    return sat_float


def convertUnix_Datetime(date):
    """Converts Unix time to Datetime Obj."""
    if date:
        return datetime.fromtimestamp(float(date))
    return getCurrentDateTime()


def getCoinMarketCapSymbolData():
    """Gets latest data from coinmarketcap module.
       Returns Dict."""

    coi = Market()
    stats = coi.stats()
    _logger.info('Getting Current market status...')

    market_size = stats['active_currencies'] + stats['active_assets']
    _logger.info(
        '{} active currencies and assets found...'.format(market_size))

    data = coi.ticker(limit=market_size)

    return data


def getTwitterHandle(coin_id):
    """
    Takes Dict with a cid
    Returns twitter handle.
    """
    url = 'https://coinmarketcap.com/currencies/{0}/#social'
    url = url.format(coin_id)

    response = requests.get(url)
    soup = BeautifulSoup(response.content, 'html.parser')

    try:
        social_section = soup.find(id='social')
        twitter_handle = social_section.find('a').text.split('@')[1]
    except AttributeError:
        return None

    return twitter_handle


def addTwitterHandles(symbol):
    handle = {'twitter': getTwitterHandle(symbol['cid'])}
    return {**symbol, **handle}


def addStatistics(symbol):
    stats = getStatistics(symbol['cid'])
    return {**symbol, **stats}


def convertKeyID_CID(symbol):
    symbol['cid'] = symbol.pop('id')


def convertRank_To_Int(symbol):
    symbol['rank'] = int(symbol['rank'])


def convertPrice_To_Int(symbol):
    symbol['price_btc'] = convertSatoshi(symbol['price_btc'])


def convertKey_Volume(symbol):
    symbol['volume_24h_usd'] = symbol.pop('24h_volume_usd')


def convertColumns_to_Int(symbol):

    symbol['volume_24h_usd'] = convertStr_Decimal_Int(
        symbol['volume_24h_usd'])

    symbol['market_cap_usd'] = convertStr_Decimal_Int(
        symbol['market_cap_usd'])

    symbol['available_supply'] = convertStr_Decimal_Int(
        symbol['available_supply'])

    symbol['total_supply'] = convertStr_Decimal_Int(symbol['total_supply'])
    symbol['max_supply'] = convertStr_Decimal_Int(symbol['max_supply'])


def getCurrentDateTime():
    return datetime.utcnow()


def cleanCoinMarketCapData(data):
    now = getCurrentDateTime()

    for symbol in data:
        convertKeyID_CID(symbol)

        convertRank_To_Int(symbol)

        convertPrice_To_Int(symbol)

        convertKey_Volume(symbol)

        convertColumns_to_Int(symbol)

        symbol['last_updated'] = convertUnix_Datetime(symbol['last_updated'])
    return data


def writeDataToCoinCapdatabase():
    start_time = time()
    data = getCoinMarketCapSymbolData()
    data = cleanCoinMarketCapData(data)  # limited for testing
    len_of_data = len(data)

    for index, symbol in enumerate(data):
        symbol = addTwitterHandles(symbol)
        symbol = addStatistics(symbol)
        CoinCapData.addCoin(**symbol)
        _logger.debug('{} out of {} coins parsed.'.format(
            index + 1, len_of_data))
        sleep(0.25)

    end_time = (time() - start_time) / 60

    _logger.info('Took {} minutes to parse/update {} coins to the database. Averaging {} seconds per coin.'.format(
        end_time, len_of_data, end_time / len_of_data))


if __name__ == '__main__':
    writeDataToCoinCapdatabase()
    # coi = Market()
    # data = coi.ticker(currency='fonziecoin')
    # data = cleanCoinMarketCapData(data)
    # print(data)
    # symbol = addTwitterHandles(data[0])
    # symbol = addStatistics(data[0])
    # CoinCapData.addCoin(**symbol)
