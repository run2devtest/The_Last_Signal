from datetime import date

from urllib.error import HTTPError

import pandas as pd


def getVolumeStatistics(dataframe, cid):
    """Gets statistics from volume data.

    :param dataframe: pandas.DataFrame
    :param cid: Coin Identification
    :return: dict()
    """

    df = dataframe
    if not df['Volume'].isnull().all():
        volume = df['Volume'].replace('-', None).astype(int)

        _mean = int(volume.mean())

        try:
            _std = int(volume.std())
        except ValueError:
            _std = 0

        _min = int(volume.min())
        _max = int(volume.max())
        _bars = int(volume.count())
        _last = int(volume[0])

        print('Successfully parsed {} volume statistics.'.format(cid))

        return {'mean_volume': _mean, 'std_volume': _std, 'lowest_volume':
                _min, 'highest_volume': _max, 'bars_recorded': _bars, 'last_volume': _last}
    else:
        return {'mean_volume': 0, 'std_volume': 0, 'lowest_volume': 0,
                'highest_volume': 0, 'bars_recorded': 0, 'last_volume': 0}


def getRangeStatistics(dataframe, cid, period):
    """Gets range statistics

    :param dataframe: pandas.DataFrame
    :param cid: Coin Identification
    :param period: int
    :return: dict() 
    """
    name = 'd' + str(period)
    df = dataframe

    if not df['High'].isnull().all():
        if not df['Low'].isnull().all():
            if not df['Close'].isnull().all():
                today = pd.Timestamp.now()

                df.index = pd.to_datetime(df.Date, format='%b %d, %Y')
                sliced_period = df.head(period)

                _llow = float(sliced_period['Low'].min())
                _hhigh = float(sliced_period['High'].max())
                _lclose = float(sliced_period['Close'][0])

                _llday = int((today - sliced_period['Low'].idxmin()).days)
                _hhday = int((today - sliced_period['High'].idxmax()).days)

                _pumpp = int(((_hhigh - _llow) / _llow) * 100)
                _lrange = int(((_lclose - _llow) / _llow) * 100)
                _hrange = int(((_lclose - _hhigh) / _hhigh) * 100)

                print('Successfully parsed {} price {}range statistics.'.format(
                    cid, period))

                return ({'{}_days_since_ll'.format(name): _llday,
                         '{}_days_since_hh'.format(name): _hhday,

                         '{}_lowest_low'.format(name): _llow,
                         '{}_highest_hi'.format(name): _hhigh,
                         '{}_last_close'.format(name): _lclose,
                         '{}_pump_range'.format(name): _pumpp,
                         '{}_lo_range'.format(name): _lrange,
                         '{}_hi_range'.format(name): _hrange})
    else:
        return ({'{}_days_since_ll'.format(name): 0,
                 '{}_days_since_hh'.format(name): 0,

                 '{}_lowest_low'.format(name): 0,
                 '{}_highest_hi'.format(name): 0,
                 '{}_last_close'.format(name): 0,
                 '{}_pump_range'.format(name): 0,
                 '{}_lo_range'.format(name): 0,
                 '{}_hi_range'.format(name): 0})


def getStatistics(cid):
    """Parses Volume/Range Statistics from coinmarketcap historical-data page.

    :param cid: Coin Identification
    :return: dict()
    """
    try:
        url = 'https://coinmarketcap.com/currencies/{cid}/historical-data/?start={start}&end={end}'
        start = date(2013, 4, 28).isoformat().replace('-', '')
        end = date.today().isoformat().replace('-', '')
        url = url.format(cid=cid, start=start, end=end)

        df = pd.read_html(url)[0]
        volume_stats = getVolumeStatistics(df, cid)
        range_30 = getRangeStatistics(df, cid, 30)
        range_90 = getRangeStatistics(df, cid, 90)
        range_180 = getRangeStatistics(df, cid, 180)
        return {**volume_stats, **range_30, **range_90, **range_180}

   # check if No data

    except HTTPError:
        print('Unable to parse {} volume statistics.'.format(cid))
        return None


if __name__ == '__main__':
    print(getStatistics('poet'))
    print(getStatistics('bit20'))
    print(getStatistics('fonziecoin'))
