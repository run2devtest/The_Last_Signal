from os import path

BASE_PATH = path.dirname(path.abspath(__file__))


def GetToken(*args):
    """Parses TOKEN file. Strips newlines and
    whitespaces. Returns token string.
    """
    if not args:
        file = 'TOKEN'
    else:
        file = args[0]

    try:

        with open(path.join(BASE_PATH, file), 'r') as TOKEN:
            return TOKEN.read().strip('\n').strip(' ')
    except FileNotFoundError:
        print('Please Add {}'.format(file))
        return None


if __name__ == '__main__':
    print(GetToken())
