import telegram
from telegram.error import TimedOut, BadRequest, TelegramError
from time import sleep
from notifiers.gettoken import GetToken


CHANNEL = '-1001322139147'


def htmlMessageFix(message):
    if 'Empty DataFrame' not in message:
        return '<pre>{}</pre>'.format(message)
    return '<pre>No Data Avaliable</pre>'


def sendMessage(message):
    bot = telegram.Bot(GetToken())
    message = '<pre> {} </pre>'.format(message)

    try:
        bot.send_message(chat_id=CHANNEL, text=message,
                         parse_mode=telegram.ParseMode.HTML)
        sleep(10)
    except BadRequest:
        print('Chat Not Found')
    except TelegramError:
        print('Invalid Server response.')
    except TimedOut:
        print('Bot TimedOut')
