import socket


def send_voice_msg(message):
    # create an ipv4 (AF_INET) socket object using the tcp protocol
    # (SOCK_STREAM)
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # connect the client
    try:
        client.connect(('0.0.0.0', 6666))

        # send some data (in this case a HTTP GET request)
        client.send(message.encode())

        # receive the response data (4096 is recommended buffer size)
        response = client.recv(4096)

        if response.decode() == 'ACK!':
            print('message sent.')
        else:
            print('message not acknowledged.')
    except ConnectionRefusedError:
        print('meesage not sent.')

if __name__ == '__main__':
    send_voice_msg('testing')
