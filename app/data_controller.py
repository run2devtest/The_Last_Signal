from model import CoinCapTwitterData
from filters import getSogFilterCoins, getSogFilterPlusCoins, getBillionFilterCoins, get500MillionFilterCoins, getLowVolFilterShitCoins
from filters import getLessThan100MillionFilterCoins, getLessThan10MillionFilterCoins
from filters import getBillionFilterCoinsNoBinace, get500MillionFilterCoinsNoBinace

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from matplotlib import style
from collections import Counter
from os import path, makedirs

from datetime import datetime
from PIL import Image, ImageOps
from logger import GetLogger

_logger = GetLogger()

BASE_PATH = path.dirname(path.abspath(__file__))
CHART_PATH = path.join(BASE_PATH, 'charts')
LOGO_PATH = path.join(BASE_PATH, '/img/logo.png')
PICKLE_PATH = path.join(BASE_PATH, 'DATA.pickle')


def create_pickle():
    # [:100]# used for testing.
    coins = [x.cid_id for x in CoinCapTwitterData.getDistictCoinsCids()]
    df = pd.DataFrame()
    for count, x in enumerate(coins):
        followers = []
        index = []
        for y in CoinCapTwitterData.getFollowersByCid(x):
            followers.append(y.followers)
            index.append(y.created_date)
        if count == 0:
            df = pd.DataFrame(index=index, data=followers, columns=[x])
        else:
            df = df.merge(pd.DataFrame(
                index=index, data=followers, columns=[x]), right_index=True, left_index=True, how='outer')

        _logger.info('pickling {} out of {} coins processed.'.format(
            count + 1, len(coins)))

    df.fillna(method='ffill', inplace=True)
    df.fillna(method='bfill', inplace=True)
    df.to_pickle(PICKLE_PATH)
    _logger.info('Pickle complete')


def get_pickle():
    return pd.read_pickle(PICKLE_PATH)


def print_list(dframe, **kwargs):
    min_tweet_size = kwargs['min_tweet_size']
    resample_size = kwargs['resample_size']
    return_size = kwargs['return_size']

    latest = dframe[-1:]
    bools = (latest > min_tweet_size).any(axis=0)
    keep = list(latest.loc[:, bools].columns)
    pct = dframe[keep]
    pct = pct.resample(resample_size).mean().pct_change()

    row = pct[-1:].reset_index().drop('index', axis=1)
    row = row.melt(var_name='coin')
    row = row.sort_values('value').rename(columns={'value': 'percent_change'})
    row['percent_change'] = row['percent_change'] * 100
    return list(row['coin'].tail(return_size))


def create_filtered_plot(filter_, algo_name, file_name, chart_bg_color='lightgray', experimental=False):
    df = get_pickle()[filter_()]

    dlist = []
    for size in [500, 1000, 2500, 5000, 10000, 25000, 50000]:
        dlist += print_list(df, min_tweet_size=size,
                            resample_size='3H', return_size=5)
        dlist += print_list(df, min_tweet_size=size,
                            resample_size='6H', return_size=5)
        dlist += print_list(df, min_tweet_size=size,
                            resample_size='12H', return_size=5)
        dlist += print_list(df, min_tweet_size=size,
                            resample_size='1D', return_size=5)
        dlist += print_list(df, min_tweet_size=size,
                            resample_size='3D', return_size=5)
        dlist += print_list(df, min_tweet_size=size,
                            resample_size='5D', return_size=5)
        dlist += print_list(df, min_tweet_size=size,
                            resample_size='7D', return_size=5)

    counter = Counter(dlist)
    size = int(len(counter))
    thresh = int(size / 2)
    dlist = [x[0] for x in counter.most_common()[:thresh]]
    plot_coin(df, dlist, algo_name, file_name, size, chart_bg_color)

    if experimental:
        # main difference between normal and experimental
        dlist = [x[0] for x in counter.most_common()[thresh:]]
        plot_coin_experimental(df, dlist, algo_name, file_name, size)


def get_number_one():
    filter = getBillionFilterCoins()
    df = get_pickle()[filter]
    # df = get_pickle()
    hitterlist = []
    for x in ['1D', '2D', '3D', '4D', '5D', '6D', '7D']:
        hitterlist.append(print_list(df, min_tweet_size=100,
                                     resample_size=x, return_size=1)[0])
    return ', '.join(list(set(hitterlist)))


def plot_coin(dataframe, coin_list, algo_name, file_name, samp_size, chart_bg_color='lightgray'):

    sns.set(style='darkgrid')
    color = sns.color_palette('Paired', samp_size)
    sns.set_palette(color)
    title = 'SOG31 ALGORITHM FILTERED BY \n[{}] \nTwitter:@THESOG31 \nPROCESSED {} UTC'.format(
        algo_name, datetime.utcnow())

    fig, axes = plt.subplots(nrows=2, squeeze=True, figsize=(7, 10))

    df = dataframe[coin_list]
    ax1 = df.resample('1D').mean().pct_change().tail(
        1).plot(kind='bar', title='1 DAY ACTIVITY', use_index=False, ax=axes[0])

    normalized = (df - df.mean()) / (df.max() - df.min())
    line_style = ['-', '--', '-*', ':', ':', '--', '-'] * samp_size

    ax2 = normalized.resample('3H').mean().tail(28).plot(
        title='3 DAY ACTIVITY', ax=axes[1], style=line_style)

    fig.suptitle(title, fontsize=10, fontweight='bold')
    ax1.legend(loc=2, fontsize=8, frameon=True, shadow=True)
    ax1.set_facecolor('black')
    ax1.set_xticks([])
    ax1.set_yticks([])

    ax2.legend(loc=2, fontsize=8, frameon=True, shadow=True)
    ax2.set_facecolor('black')
    ax2.set_yticks([])
    ax2.set_xlabel('UTC DATE TIME (+00:00)')

    # ADDING WATERMARK
    if path.exists(LOGO_PATH):
        logo = Image.open(LOGO_PATH)
        logo = ImageOps.invert(logo)
        logo.thumbnail((128, 128), Image.ANTIALIAS)
        fig.figimage(logo, 0, 0, alpha=.25)
        fig.figimage(logo, 575, 0, alpha=.25)
        fig.figimage(logo, 575, 875, alpha=.25)
        fig.figimage(logo, 0, 875, alpha=.25)

    if not path.exists(CHART_PATH):
        makedirs(CHART_PATH)

    file_path = path.join(CHART_PATH, '{}.png'.format(file_name))

    fig.savefig(file_path, facecolor=chart_bg_color)


def plot_coin_experimental(dataframe, coin_list, algo_name, file_name, samp_size, chart_bg_color='grey'):

    sns.set(style='darkgrid')
    color = sns.color_palette('Paired', samp_size)
    sns.set_palette(color)
    title = 'SOG31 ALGORITHM FILTERED BY \n[{}] experimental \nTwitter:@THESOG31 \nPROCESSED {} UTC'.format(
        algo_name, datetime.utcnow())

    fig, axes = plt.subplots(nrows=2, squeeze=True, figsize=(7, 10))

    df = dataframe[coin_list]
    ax1 = df.resample('1D').mean().pct_change().tail(
        1).plot(kind='bar', title='1 DAY ACTIVITY', use_index=False, ax=axes[0])

    normalized = (df - df.mean()) / (df.max() - df.min())
    line_style = ['-', '--', '-*', ':', ':', '--', '-'] * samp_size

    ax2 = normalized.resample('3H').mean().tail(28).plot(
        title='3 DAY ACTIVITY', ax=axes[1], style=line_style)

    fig.suptitle(title, fontsize=10, fontweight='bold')
    ax1.legend(loc=2, fontsize=8, frameon=True, shadow=True)
    ax1.set_facecolor('black')
    ax1.set_xticks([])
    ax1.set_yticks([])

    ax2.legend(loc=2, fontsize=8, frameon=True, shadow=True)
    ax2.set_facecolor('black')
    ax2.set_yticks([])
    ax2.set_xlabel('UTC DATE TIME (+00:00)')

    # ADDING WATERMARK
    if path.exists(LOGO_PATH):
        logo = Image.open(LOGO_PATH)
        logo = ImageOps.invert(logo)
        logo.thumbnail((128, 128), Image.ANTIALIAS)
        fig.figimage(logo, 0, 0, alpha=.25)
        fig.figimage(logo, 575, 0, alpha=.25)
        fig.figimage(logo, 575, 875, alpha=.25)
        fig.figimage(logo, 0, 875, alpha=.25)

    if not path.exists(CHART_PATH):
        makedirs(CHART_PATH)

    file_path = path.join(CHART_PATH, '{}experimental.png'.format(file_name))

    fig.savefig(file_path, facecolor=chart_bg_color)


def create_Charts(new_data=False, experimental=False):
    if new_data:
        create_pickle()

    create_filtered_plot(getBillionFilterCoins,
                         'COINS ON EXCHANGES WITH VOLUME > $1B', 'ONEBILLION', experimental=experimental, chart_bg_color='green')
    create_filtered_plot(get500MillionFilterCoins,
                         'COINS ON EXCHANGES WITH VOLUME > $500M', '500MILLION', experimental=True, chart_bg_color='yellow')

    create_filtered_plot(getBillionFilterCoinsNoBinace,
                         'COINS ON EXCHANGES WITH VOLUME > $1B NO BINANCE/HITBTC', 'ONEBILLION_NO_BINANCE_HITBTC', experimental=True, chart_bg_color='green')
    create_filtered_plot(get500MillionFilterCoinsNoBinace,
                         'COINS ON EXCHANGES WITH VOLUME > $500M NO BINANCE/HITBTC', '500MILLION_NO_BINANCE_HITBTC', experimental=experimental, chart_bg_color='yellow')

    create_filtered_plot(getLessThan10MillionFilterCoins,
                         'LOW VOLUME SHITCOIN EXCHANGES < $10M', 'SHITCOIN', experimental=experimental, chart_bg_color='brown')
    create_filtered_plot(getLessThan100MillionFilterCoins,
                         'LOW VOLUME SHITCOIN EXCHANGES < $100M', 'SHITCOIN2', experimental=experimental, chart_bg_color='brown')

    print('done')

if __name__ == '__main__':
    # create_Charts(new_data=False)
    print(get_number_one())
