Initial setup

Install postgres

pip install -r REQUIREMENTS

create {} database

sudo apt install python3-tk

python3 config.py

python3 start_cron.py

STEPS to dump and recover postgresDB

How to create a postgres dump

- pg_dump the_sauce > the_sauce_dump

How to recover a postgres dump

- psql the_sauce2 < the_sauce_dump3

If copying from old database change First Run setting to False